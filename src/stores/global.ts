import { ref } from "vue";
import { defineStore } from "pinia";

export const useGlobalStore = defineStore("global", {
  state: () => {
	return {
		year: new Date().getFullYear(),
	 	animating: false,
	 	loaded: ref(false),
	}
  },
  getters: {
	isAppLoaded() {
		return this.loaded
	},
	isAppAnimating() {
		return this.animating
	}
  },
  actions: {
    setAppLoaded() {
      this.loaded = true
    },
    setAppAnimating() {
      this.animating = !this.animating
    },
  }
})
